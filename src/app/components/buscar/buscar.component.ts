import { Component, OnInit } from '@angular/core';
import { PeliculasService } from '../../services/peliculas.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styles: []
})
export class BuscarComponent implements OnInit {

  buscar:string = "";

  constructor( private _ps:PeliculasService,
                public route:ActivatedRoute ) {

     this.route.params.subscribe( parametro => {
       console.log(parametro);
       if( parametro['parametro'] ){
         this.buscar = parametro['parametro']
         this.buscarPelicula();
       }
     })

  }

  ngOnInit() {
  }

  buscarPelicula(){

    if( this.buscar.length == 0){
      return;
    }else{
      this._ps.searchMovie(this.buscar)
          .subscribe()
    }
  }

}
