import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'peliculaImagen'
})
export class PeliculaImagenPipe implements PipeTransform {

  transform( value: any, poster:boolean = false ):any {

    let url = "http://image.tmdb.org/t/p/w500";

    if( poster ) {
      if( value.poster_path ){
        return url + value.poster_path
        }else{
        return "assets/img/no-image.png"
      }
    }
    if( value.backdrop_path ){
      return url + value.backdrop_path;
    }else{
      if( value.poster_path ){
        return url + value.poster_path
      }else{
        return "assets/img/no-image.png"
      }
    }
}

}
