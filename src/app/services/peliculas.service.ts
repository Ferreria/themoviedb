import { Injectable } from '@angular/core';
import { Jsonp, Http } from '@angular/http';
import 'rxjs/Rx';
import { DatePipe } from '@angular/common';

@Injectable()
export class PeliculasService {

  peliculas:any[] = [];

  private apiKey:string = "87bb5e8216353013e7910284806e22e5";
  private urlMoviedb:string = "https://api.themoviedb.org/3";

  constructor( private jsonp:Jsonp, private datePipe:DatePipe ) { }


  getPopulares(){


    let url = `${ this.urlMoviedb }/discover/movie?sort_by=popularity.desc&api_key=${ this.apiKey }&language=es&callback=JSONP_CALLBACK`;


    return this.jsonp.get( url )
            .map( res => res.json().results );

  }

  getCartelera(){

    let desde = new Date();
    let hasta= new Date();
    hasta.setDate( hasta.getDate() + 7 );

    let desdeStr = this.datePipe.transform(desde,"yyyy-MM-dd");
    let hastaStr = this.datePipe.transform(hasta,"yyyy-MM-dd");

    let url = `${ this.urlMoviedb }/discover/movie?primary_release_date.gte=${desdeStr}&primary_release_date.lte=${hastaStr}&api_key=${ this.apiKey }&language=es&callback=JSONP_CALLBACK`;

    return this.jsonp.get( url )
            .map( res => res.json().results );

  }

  getPopularesKid(){

    let url = `${ this.urlMoviedb }/discover/movie?certification_country=US&certification.lte=G&sort_by=popularity.desc&api_key=${ this.apiKey }&language=es&callback=JSONP_CALLBACK`;


    return this.jsonp.get( url )
            .map( res => res.json().results );

  }

  searchMovie( parametro:string ){

    let url = `${ this.urlMoviedb }/search/movie?query=${ parametro }&sort_by=popularity.desc&api_key=${ this.apiKey }&language=es&callback=JSONP_CALLBACK`;


    return this.jsonp.get( url )
            .map( res => {
              this.peliculas = res.json().results,
              console.log(this.peliculas);

              return res.json().results
            })

  }

  getPelicula( id:string ){

    let url = `${ this.urlMoviedb }/movie/${ id }?&api_key=${ this.apiKey }&language=es&callback=JSONP_CALLBACK`;


    return this.jsonp.get( url )
            .map( res => res.json() );

  }


}
